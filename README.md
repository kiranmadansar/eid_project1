# Repo for Project1, Embedded Interface Design
Temperature Sensor interface to Raspberry Pi and user interface using pyQT
 
Submitted by: Kiran Hegde

### Prerequisites: 
PyQt5 should be installed before running this project
Python3 is required

### Installation: 
Clone this repository and run qt5project.py using python3.

'
python3 qt5project.py <pin>
'

Note: <pin> is the GPIO number to which the sensor is connected 

### Project Description:
Used Adafruit DHT 22 temperature sensor to measure temperature and humidity. 
A simple GUI is built using PyQt to provide different features like setting alerts, getting recent values, getting temperature in C, F or K.    


### Project Additions:
- Option has been provided to switch between degree Celsius, Kelvin and Fahranheit
- Timer has been used to get the data at regular intervals
- A critical Message shows up if sensor is not connected
- We can set up the alert for min and max temperature 

### References:
1. http://wiki.qt.io/Raspberry_Pi_Beginners_Guide
2. https://pythonspot.com/en/pyqt5/
3. https://www.tutorialspoint.com/pyqt/
4. Bruce Montgomery Lecture Slides 
5. https://www.tutorialspoint.com/pyqt/pyqt_qlineedit_widget.htm
6. https://www.adafruit.com/product/385
