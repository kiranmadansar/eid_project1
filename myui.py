# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Project1.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(545, 419)
        self.pushButton_E = QtWidgets.QPushButton(Dialog)
        self.pushButton_E.setGeometry(QtCore.QRect(430, 370, 101, 31))
        self.pushButton_E.setObjectName("pushButton_E")
        self.label_t = QtWidgets.QLabel(Dialog)
        self.label_t.setGeometry(QtCore.QRect(240, 80, 101, 31))
        self.label_t.setObjectName("label_t")
        self.lcdNumber_t = QtWidgets.QLCDNumber(Dialog)
        self.lcdNumber_t.setGeometry(QtCore.QRect(220, 110, 141, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        font.setKerning(False)
        self.lcdNumber_t.setFont(font)
        self.lcdNumber_t.setObjectName("lcdNumber_t")
        self.label_h = QtWidgets.QLabel(Dialog)
        self.label_h.setGeometry(QtCore.QRect(420, 80, 101, 31))
        self.label_h.setObjectName("label_h")
        self.lcdNumber_h = QtWidgets.QLCDNumber(Dialog)
        self.lcdNumber_h.setGeometry(QtCore.QRect(390, 110, 141, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        font.setKerning(False)
        self.lcdNumber_h.setFont(font)
        self.lcdNumber_h.setObjectName("lcdNumber_h")
        self.label_time = QtWidgets.QLabel(Dialog)
        self.label_time.setGeometry(QtCore.QRect(280, 20, 191, 31))
        self.label_time.setObjectName("label_time")
        self.CF_Select = QtWidgets.QComboBox(Dialog)
        self.CF_Select.setGeometry(QtCore.QRect(220, 160, 141, 31))
        self.CF_Select.setObjectName("CF_Select")
        self.CF_Select.addItem("")
        self.CF_Select.addItem("")
        self.CF_Select.addItem("")
        self.getval_button = QtWidgets.QPushButton(Dialog)
        self.getval_button.setGeometry(QtCore.QRect(300, 230, 181, 31))
        self.getval_button.setObjectName("getval_button")
        self.temp_min = QtWidgets.QLineEdit(Dialog)
        self.temp_min.setGeometry(QtCore.QRect(30, 110, 113, 33))
        self.temp_min.setObjectName("temp_min")
        self.temp_max = QtWidgets.QLineEdit(Dialog)
        self.temp_max.setGeometry(QtCore.QRect(30, 180, 113, 33))
        self.temp_max.setObjectName("temp_max")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(40, 220, 101, 21))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(40, 80, 101, 21))
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(30, 270, 101, 31))
        self.pushButton.setObjectName("pushButton")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton_E.setText(_translate("Dialog", "Exit"))
        self.label_t.setText(_translate("Dialog", "Temperature"))
        self.label_h.setText(_translate("Dialog", "Humidity (%)"))
        self.label_time.setText(_translate("Dialog", "Date/Time"))
        self.CF_Select.setItemText(0, _translate("Dialog", "Celsius"))
        self.CF_Select.setItemText(1, _translate("Dialog", "Kelvin"))
        self.CF_Select.setItemText(2, _translate("Dialog", "Fahranheit"))
        self.getval_button.setText(_translate("Dialog", "Get Values"))
        self.label.setText(_translate("Dialog", "Temp Max (C)"))
        self.label_2.setText(_translate("Dialog", "Temp Min (C)"))
        self.pushButton.setText(_translate("Dialog", "Set Alarm"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

