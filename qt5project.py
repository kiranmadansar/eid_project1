"""

UNIVERSITY OF COLORADO BOULDER

Embedded Interface Design
Project 1

Author: Kiran Hegde
Date: 09/30/2018

"""
import sys
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QWidget, QDialog, QMessageBox 
from PyQt5.QtGui import QIcon
from datetime import datetime
from myui import Ui_Dialog
import Adafruit_DHT
import threading

cel_far = 0		#0 for celcius and 1 for fahranheit
time_interval = 20	#interval for timer in seconds
alarm = 0		#0 for alarm disable and 1 for enable
tempmin=0.0		#min temp threshold in degree Celsius
tempmax=0.0		#max temp threshold

class Applicat(object):
	def update_val(self):
		global cel_far, time_interval, tempmin, tempmax
		hum, temp = Adafruit_DHT.read(sensor, pin)	#read value from temperature
		timec=datetime.now().strftime('%b-%d-%Y %H:%M:%S') #get time
		ui.label_time.setText(timec) #show the time on label 
		if temp and hum is not None:
			temp1 = temp
			if cel_far == 0:
				ui.lcdNumber_t.display(temp) #display Celsius
			elif cel_far == 1:
				temp = (temp*1.8)+32
				ui.lcdNumber_t.display(temp) #display fahranheit
			else:
				temp = temp+273.15
				ui.lcdNumber_t.display(temp) #display kelvin
			ui.lcdNumber_h.display(hum) # display humidity
			if (alarm == 1 and temp1 < tempmin): 
				self.alarm_min() #alarm for minimum temp threshold
			if (alarm == 1 and temp1 > tempmax):
				self.alarm_max() #alarm for maximum temp threshold
			print("Got values")
		else:
			ui.lcdNumber_t.display("ERROR")
			ui.lcdNumber_h.display("ERROR")
			self.showError() #sensor not connected error
			print("Error")

	def showError(self):
		msg = QMessageBox() #message box
		msg.setIcon(QMessageBox.Critical) # associate the icon
		msg.setText("Check the sensor connection")
		msg.setStandardButtons(QMessageBox.Ok)
		msg.show()
		msg.buttonClicked.connect(self.msgbutton) #if ok button is pressed, call msgbutton function
		msg.exec_()

	def msgbutton(self):
		print ("Button clicked")

	def alarm_min(self):
		msg = QMessageBox()
		msg.setIcon(QMessageBox.Critical)
		msg.setText("ALARM for Min. Temperature") # message box for minimum temp
		msg.setStandardButtons(QMessageBox.Ok)
		msg.show()
		msg.buttonClicked.connect(self.msgbutton)
		msg.exec_()

	def alarm_max(self):
		msg = QMessageBox()
		msg.setIcon(QMessageBox.Critical)
		msg.setText("ALARM for Max. Temperature") #message box for max temp
		msg.setStandardButtons(QMessageBox.Ok)
		msg.show()
		msg.buttonClicked.connect(self.msgbutton)
		msg.exec_()

	def Alarm(self):
		global alarm, tempmin, tempmax
		if alarm == 0:
			min = (ui.temp_min.text()) # get min temp value
			tempmin = float(min)
			max = (ui.temp_max.text()) # get max temp value
			tempmax = float(max)
			alarm = 1
			ui.pushButton.setText("Unset Alarm") #change the button label
		else:
			alarm = 0
			ui.pushButton.setText("Set Alarm")
 
	def index_changed(self):
		global cel_far #check if the index is changed
		if (ui.CF_Select.currentText() == "Celsius"):
			cel_far = 0
			print ("Celsius")
		elif (ui.CF_Select.currentText() == "Fahranheit"):
			cel_far = 1
			print("Fahranheit")
		else:
			cel_far = 2
			print ("Kelvin")

	def exit_pgm(self):
		timer.stop() # this function executes when exit is pressed
		sys.exit(app.exec_())



if __name__=="__main__":
	if (len(sys.argv) < 2):
		print ("\nUsage: python3 qt5project.py <pin>\n") #check if pin is entered
		sys.exit(0)
	App = Applicat()
	sensor = Adafruit_DHT.DHT22 # associate the sensor 
	pin = sys.argv[1]
	app = QApplication(sys.argv)
	Dialog = QDialog()
	ui = Ui_Dialog()
	ui.setupUi(Dialog)
	Dialog.show()
	if (ui.CF_Select.currentText() == "Celsius"): #initially it will be in celsius
		cel_far = 0
	elif (ui.CF_Select.currentText() == "Fahranheit"):
		cel_far = 1
	else:
		cel_far = 2
	print (cel_far)
	timer = QTimer() #timer for continuous data aquisition
	timer.timeout.connect(App.update_val)
	timer.start(15000) #get the values for every 15 seconds 
	time=datetime.now().strftime('%b-%d-%Y %H:%M:%S')
	ui.label_time.setText(time) # show the time
	ui.pushButton_E.clicked.connect(App.exit_pgm) # when exit is pressed
	ui.getval_button.clicked.connect(App.update_val) #when get val button is pressed
	ui.pushButton.clicked.connect(App.Alarm) #alaram button
	ui.CF_Select.currentIndexChanged.connect(App.index_changed) # when temperature format is changed 
	sys.exit(app.exec_())
